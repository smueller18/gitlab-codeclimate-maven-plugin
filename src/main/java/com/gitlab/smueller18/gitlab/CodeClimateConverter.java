package com.gitlab.smueller18.gitlab;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gitlab.smueller18.gitlab.checkstyle.json.Checkstyle;
import com.gitlab.smueller18.gitlab.checkstyle.json.Lines;
import com.gitlab.smueller18.gitlab.checkstyle.json.Location;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

@Mojo(name = "codeclimate", defaultPhase = LifecyclePhase.VALIDATE)
public class CodeClimateConverter extends AbstractMojo {

    private static final Logger logger = LogManager.getLogger(CodeClimateConverter.class);

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    MavenProject project;

    /**
     * Specifies the path and filename to save the Checkstyle output.
     */
    @Parameter(property = "checkstyle.output.file", defaultValue = "${project.build.directory}/checkstyle-result.xml")
    File inputFile;

    @Parameter(property = "codeclimate.output.file", defaultValue = "${project.build.directory}/codeclimate.json")
    File outputFile;

    public void execute() throws MojoExecutionException {
        if (!inputFile.exists()) {
            throw new RuntimeException(String.format("Checkstyle output file %s not found.", inputFile));
        }
        try {
            convertXmlToJson();
        } catch (IOException e) {
            throw new RuntimeException("Failed converting checkstyle output file to codeclimate format.", e);
        }
    }

    public static String convertMavenSeverityToCheckstyle(String severity) {
        switch(severity){
            case "error":
                return "critical";
            case "info":
            case "ignored":
                return "info";
            case "warning":
                return "major";
        }
        return "";
    }

    private void convertXmlToJson() throws IOException {

        JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        XmlMapper xmlMapper = new XmlMapper(module);
        com.gitlab.smueller18.gitlab.checkstyle.xml.Checkstyle deserializedData;
        try {
            deserializedData = xmlMapper.readValue(inputFile, com.gitlab.smueller18.gitlab.checkstyle.xml.Checkstyle.class);

        } catch (JsonParseException e) {
            logger.warn(String.format("Error parsing file %s.", inputFile));
            return;
        }

        ArrayList<Checkstyle> checkstyle = new ArrayList<>();

        if (deserializedData == null || deserializedData.file == null) {
            logger.warn(String.format("File %s does not contain any linting result.", inputFile));
            return;
        }
        Arrays.asList(deserializedData.file).forEach(file -> {
            if (file.error == null) {
                return;
            }
            Arrays.asList(file.error).forEach(error -> {
                checkstyle.add(
                        new com.gitlab.smueller18.gitlab.checkstyle.json.Checkstyle() {{
                            description = error.message;
                            fingerprint = DigestUtils.sha256Hex(file.name + error.message + error.line);
                            location = new Location() {{
                                lines = new Lines() {{
                                    begin = error.line;
                                }};
                                path = file.name.replace(project.getBasedir().getAbsolutePath() + "/", "");
                            }};
                            severity = convertMavenSeverityToCheckstyle(error.severity);
                        }}
                );
            });
        });

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.writeValue(outputFile, checkstyle);

        logger.info(String.format("Written codeclimate output file to %s", outputFile));
    }
}
