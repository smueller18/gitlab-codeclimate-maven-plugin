package com.gitlab.smueller18.gitlab.checkstyle.xml;

public class Error {
    public String severity;

    public Integer line;

    public String column;

    public String source;

    public String message;

    public String content;

}
