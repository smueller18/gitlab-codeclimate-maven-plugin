package com.gitlab.smueller18.gitlab;

import org.apache.maven.plugin.testing.MojoRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

public class CodeClimateConverterTest {

    private CodeClimateConverter projectFull = new CodeClimateConverter();
    private CodeClimateConverter projectNoErrors = new CodeClimateConverter();
    private CodeClimateConverter projectEmptyList = new CodeClimateConverter();
    private CodeClimateConverter projectEmptyFile = new CodeClimateConverter();

    @Rule
    public MojoRule rule = new MojoRule() {
        @Override
        protected void before() throws Throwable {
        }

        @Override
        protected void after() {
        }
    };

    @Before
    public void initialize() throws Exception {
        File pom = new File("src/test/resources/project-full/");
        Assertions.assertNotNull(pom);
        Assertions.assertTrue(pom.exists());
        projectFull = (CodeClimateConverter) rule.lookupConfiguredMojo(pom, "codeclimate");
        Assertions.assertNotNull(projectFull);
        if (!Files.exists(Path.of(projectFull.project.getBuild().getDirectory())))
            Files.createDirectories(Path.of(projectFull.project.getBuild().getDirectory()));

        pom = new File("src/test/resources/project-no-errors/");
        Assertions.assertNotNull(pom);
        Assertions.assertTrue(pom.exists());
        projectNoErrors = (CodeClimateConverter) rule.lookupConfiguredMojo(pom, "codeclimate");
        Assertions.assertNotNull(projectNoErrors);
        if (!Files.exists(Path.of(projectNoErrors.project.getBuild().getDirectory())))
            Files.createDirectories(Path.of(projectNoErrors.project.getBuild().getDirectory()));

        pom = new File("src/test/resources/project-empty-list/");
        Assertions.assertNotNull(pom);
        Assertions.assertTrue(pom.exists());
        projectEmptyList = (CodeClimateConverter) rule.lookupConfiguredMojo(pom, "codeclimate");
        Assertions.assertNotNull(projectEmptyList);
        if (!Files.exists(Path.of(projectEmptyList.project.getBuild().getDirectory())))
            Files.createDirectories(Path.of(projectEmptyList.project.getBuild().getDirectory()));

        pom = new File("src/test/resources/project-empty-file/");
        Assertions.assertNotNull(pom);
        Assertions.assertTrue(pom.exists());
        projectEmptyFile = (CodeClimateConverter) rule.lookupConfiguredMojo(pom, "codeclimate");
        Assertions.assertNotNull(projectEmptyFile);
        if (!Files.exists(Path.of(projectEmptyFile.project.getBuild().getDirectory())))
            Files.createDirectories(Path.of(projectEmptyFile.project.getBuild().getDirectory()));
    }

    @Test
    public void execute() throws Exception {
        projectFull.inputFile = new File(projectFull.project.getBasedir().getAbsoluteFile() + "/checkstyle-result.xml");
        projectFull.execute();
        Assertions.assertEquals(
                Files.readString(Path.of(projectFull.project.getBasedir().getAbsoluteFile().getAbsolutePath() + "/codeclimate.json")),
                Files.readString(projectFull.outputFile.toPath())
        );

        projectNoErrors.inputFile = new File(projectNoErrors.project.getBasedir().getAbsoluteFile() + "/checkstyle-result.xml");
        projectNoErrors.execute();
        Assertions.assertEquals(
                Files.readString(Path.of(projectNoErrors.project.getBasedir().getAbsoluteFile().getAbsolutePath() + "/codeclimate.json")),
                Files.readString(projectNoErrors.outputFile.toPath())
        );

        projectEmptyList.inputFile = new File(projectEmptyList.project.getBasedir().getAbsoluteFile() + "/checkstyle-result.xml");
        projectEmptyList.execute();
        assert !Files.exists(projectEmptyList.outputFile.toPath());

        projectEmptyFile.inputFile = new File(projectEmptyFile.project.getBasedir().getAbsoluteFile() + "/checkstyle-result.xml");
        projectEmptyFile.execute();
        assert !Files.exists(projectEmptyFile.outputFile.toPath());
    }

    @Test
    public void executeMisssingInputFile() throws Exception {
        projectFull.inputFile = new File(projectFull.project.getBasedir().getAbsoluteFile() + "/not-existent.xml");
        Assertions.assertThrows(RuntimeException.class, () -> projectFull.execute());
    }

    @Test
    public void executeMisssingOutputFolder() throws Exception {
        projectFull.inputFile = new File(projectFull.project.getBasedir().getAbsoluteFile() + "/checkstyle-result.xml");
        projectFull.outputFile = new File(projectFull.project.getBasedir().getAbsoluteFile() + "/not-existent/checkstyle.xml");
        Assertions.assertThrows(RuntimeException.class, () -> projectFull.execute());
    }

    @ParameterizedTest
    @CsvSource({
            "error,critical",
            "info,info",
            "ignored,info",
            "warning,major",
            "test,''",
            "'',''"
    })
    public void convertMavenSeverityToCheckstyle(String mavenSeverity, String checkstyleSeverity) {
        Assertions.assertEquals(checkstyleSeverity, CodeClimateConverter.convertMavenSeverityToCheckstyle(mavenSeverity));
    }
}
