# gitlab-codeclimate-maven-plugin

[![pipeline status](https://gitlab.com/smueller18/gitlab-codeclimate-maven-plugin/badges/master/pipeline.svg)](https://gitlab.com/smueller18/gitlab-codeclimate-maven-plugin/commits/master)
[![coverage report](https://gitlab.com/smueller18/gitlab-codeclimate-maven-plugin/badges/master/coverage.svg)](https://gitlab.com/smueller18/gitlab-codeclimate-maven-plugin/commits/master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.gitlab.smueller18.gitlab/gitlab-codeclimate-maven-plugin/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.gitlab.smueller18.gitlab/gitlab-codeclimate-maven-plugin)

Plugin for transforming maven checkstyle plugin xml output to [GitLab Code Climate Spec](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#implementing-a-custom-tool).

## Requirements

-   Java >= 11
-   Maven >= 3.6.0
-   GitLab >= 12.0 Starter/Bronze ([Merge requests code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html))

## Usage

Add the plugin to your project pom and adjust configuration for your needs. Have a look at
[CodeClimateConverter.java](https://gitlab.com/smueller18/gitlab-codeclimate-maven-plugin/blob/master/src/main/java/com/gitlab/smueller18/gitlab/CodeClimateConverter.java)
to see what parameters and environment variables can be set. The order of the plugins is important.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.gitlab.smueller18</groupId>
  <artifactId>maven-test</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>jar</packaging>
  <name>Maven Test Project</name>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <version>3.1.0</version>
        <executions>
          <execution>
            <id>validate</id>
            <phase>validate</phase>
            <goals>
              <goal>check</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>com.gitlab.smueller18.gitlab</groupId>
        <artifactId>gitlab-codeclimate-maven-plugin</artifactId>
        <version>1.1.0</version>
        <executions>
          <execution>
            <id>validate</id>
            <phase>validate</phase>
            <goals>
              <goal>codeclimate</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>

</project>
```

Finally, a `.gitlab-ci.yml` file must be placed in the project roots. Here is a minimalistic one:

```yaml
stages:
  - validate

variables:
  MAVEN_CLI_OPTS: --batch-mode

validate:
  stage: validate
  image: maven:3.6.1-jdk-11
  script:
    - mvn $MAVEN_CLI_OPTS validate
  artifacts:
    reports:
      codequality: target/codeclimate.json
```

## Development

### Update dependencies

```bash
mvn versions:use-latest-versions \
  -Dmaven.version.rules=https://gitlab.com/smueller18/gitlab-codeclimate-maven-plugin/-/snippets/2088453/raw/master/maven-version-rules.xml
mvn versions:update-properties \
  -Dmaven.version.rules=https://gitlab.com/smueller18/gitlab-codeclimate-maven-plugin/-/snippets/2088453/raw/master/maven-version-rules.xml
```

### Release

- Create new tag with `git tag -a x.x.x -m "tag x.x.x"`
- Publish new tag with `git push origin x.x.x`
- Run `mvn deploy -Pgpg-sign`
- Open <https://oss.sonatype.org/> and release artifact
